from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray
import io
import sys
import unittest

class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    #2D POLJA = POZITIVNA, NEGATIVNA, POZ+NEG

    def test_Pozitivna_INT_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 2, 3, 4, 5, 6))
        b = '{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(1, 2, 3, 4, 5, 6)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), b)

    def test_Pozitivna_FLOAT_2D(self):
        a = BaseArray((2, 3), dtype=float, data=(1., 2., 3., 4., 5., 6.))
        b = '{:7.2f}{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}{:7.2f}\n'.format(1., 2., 3., 4., 5., 6.)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), b)
    
    def test_Negativna_INT_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(-1, -2, -3, -4, -5, -6))
        b = '{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(-1, -2, -3, -4, -5, -6)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), b)

    def test_Negativna_FLOAT_2D(self):
        a = BaseArray((2, 3), dtype=float, data=(-1., -2., -3., -4., -5., -6.))
        b = '{:7.2f}{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}{:7.2f}\n'.format(-1., -2., -3., -4., -5., -6.)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), b)

    def test_Mesana_INT_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(-1, 2, -3, 4, -5, 6))
        b = '{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(-1, 2, -3, 4, -5, 6)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), b)

    def test_Mesana_FLOAT_2D(self):
        a = BaseArray((2, 3), dtype=float, data=(-1., 2., -3., 4., -5., 6.))
        b = '{:7.2f}{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}{:7.2f}\n'.format(-1., 2., -3., 4., -5., 6.)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), b)

    #3 POLJA = POZITIVNA, NEGATIVNA, POZ+NEG
    def test_Pozitivna_INT_3D(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
        o = 'Plast: 0\n{:5d}{:5d}\n{:5d}{:5d}\n{:5d}{:5d}\nPlast: 1\n{:5d}{:5d}\n{:5d}{:5d}\n{:5d}{:5d}\n'.format(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

    def test_Pozitivna_FLOAT_3D(self):
        a = BaseArray((2, 3, 2), dtype=float, data=(1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12.))
        o = 'Plast: 0\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\nPlast: 1\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n'.format(1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12.)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)
    
    def test_Negativna_INT_3D(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(-1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12))
        o = 'Plast: 0\n{:5d}{:5d}\n{:5d}{:5d}\n{:5d}{:5d}\nPlast: 1\n{:5d}{:5d}\n{:5d}{:5d}\n{:5d}{:5d}\n'.format(-1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)
    
    def test_Negativna_FLOAT_3D(self):
        a = BaseArray((2, 3, 2), dtype=float, data=(-1., -2., -3., -4., -5., -6., -7., -8., -9., -10., -11., -12.))
        o = 'Plast: 0\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\nPlast: 1\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n'.format(-1., -2., -3., -4., -5., -6., -7., -8., -9., -10., -11., -12.)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

    def test_Mesana_INT_3D(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(-1, 2, -3, 4, -5, 6, -7, 8, -9, 10, -11, 12))
        o = 'Plast: 0\n{:5d}{:5d}\n{:5d}{:5d}\n{:5d}{:5d}\nPlast: 1\n{:5d}{:5d}\n{:5d}{:5d}\n{:5d}{:5d}\n'.format(-1, 2, -3, 4, -5, 6, -7, 8, -9, 10, -11, 12)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)
    
    def test_Mesana_FLOAT_3D(self):
        a = BaseArray((2, 3, 2), dtype=float, data=(-1., 2., -3., 4., -5., 6., -7., 8., -9., 10., -11., 12.))
        o = 'Plast: 0\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\nPlast: 1\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n{:7.2f}{:7.2f}\n'.format(-1., 2., -3., 4., -5., 6., -7., 8., -9., 10., -11., 12.)
        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.izpisTabele  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertMultiLineEqual(capture.getvalue(), o)

#SORTIRANJE 1D in 2D

    def test_sortiraj_1D(self):
        a = BaseArray((6, ), data=(1, -5, 3, 0, -2, -7))
        b = BaseArray((6, ), data=(-7, -5, -2, 0, 1, 3))

        a.MergeSort()
        self.assertListEqual([i for i in a], [i for i in b])

    def test_sortiraj_2D_vrstice(self):
        a = BaseArray((3, 3), data=(1, -5, 3, 0, -2, -7, 2, 1, 6))
        b = BaseArray((3, 3), data=(-5, 1, 3, -7, -2, 0, 1, 2, 6))

        a.MergeSort("vrstica")
        self.assertListEqual([i for i in a], [i for i in b])

    def test_sortiraj_2D_stolpci(self):
        a = BaseArray((3, 3), data=(1, -5, 3, 0, -2, -7, 2, 1, 6))
        b = BaseArray((3, 3), data=(0, -5, -7, 1, -2, 3, 2, 1, 6))

        a.MergeSort("stolpec")
        self.assertListEqual([i for i in a], [i for i in b])

    def test_iskanjeElementa_1D(self):
        a = BaseArray((6, ), data=(2, -5, 3, 0, 1, -7))
        b = "((5))\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(1)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)
    
    def test_iskanjeElementa_1D_multi(self):
        a = BaseArray((6, ), data=(1, -5, 3, 0, 1, -7))
        b = "((1), (5))\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(1)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)

    def test_iskanjeElementa_1D_empty(self):
        a = BaseArray((6, ), data=(1, -5, 3, 0, 1, -7))
        b = "Vrednost ni bila najdena\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(2)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)

    def test_iskanjeElementa_2D(self):
        a = BaseArray((3, 2), data=(2, -5, 3, 0, 1, -7))
        b = "((3, 1))\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(1)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)
    
    def test_iskanjeElementa_2D_multi(self):
        a = BaseArray((3, 2), data=(1, -5, 3, 0, 1, -7))
        b = "((1, 1), (3, 1))\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(1)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)

    def test_iskanjeElementa_2D_empty(self):
        a = BaseArray((3, 2), data=(1, -5, 3, 0, 1, -7))
        b = "Vrednost ni bila najdena\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(5)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)

    def test_iskanjeElementa_3D(self):
        a = BaseArray((3, 2, 2), data=(2, -5, 3, 10, 1, -7, 2, -5, 3, 0, 1, -7))
        b = "((1, 2, 2))\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(10)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)

    def test_iskanjeElementa_3D_multi(self):
        a = BaseArray((3, 2, 2), data=(2, -5, 3, 10, 10, -7, 2, -5, 3, 0, 10, -7))
        b = "((1, 2, 2), (2, 1, 1), (3, 2, 1))\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(10)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)

    def test_iskanjeElementa_3D_empty(self):
        a = BaseArray((3, 2, 2), data=(2, -5, 3, 10, 10, -7, 2, -5, 3, 0, 10, -7))
        b = "Vrednost ni bila najdena\n"

        capture = io.StringIO()  # Create StringIO object
        sys.stdout = capture  # and redirect stdout.
        a.iskanjeElementa(104)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capture.getvalue(), b)

    # 1. ) SESTEVANJE

    def test_sestevanje_skalar(self):
    # Sprotno testiranje tudi ohranjanje ustreznega tipa + pretvorba v float 
        # pozitivna INT, skalar INT 
        a = BaseArray((10,),dtype = int, data=(1,2,3,4,5,6,7,8,9,10))
        skalar = 1
        rezultat = BaseArray((10,),dtype = int, data=(2,3,4,5,6,7,8,9,10,11))

        sestej = BaseArray.sestevanje(a, skalar)
        self.assertListEqual([x for x in rezultat], [x for x in sestej])

        # pozitivna INT, skalar FLOAT 
        a1 = BaseArray((10,),dtype = int, data=(1,2,3,4,5,6,7,8,9,10))
        skalar1 = 1.0
        rezultat1 = BaseArray((10,),dtype = float, data=(2,3,4,5,6,7,8,9,10,11))

        sestej1 = BaseArray.sestevanje(a1, skalar1)
        self.assertListEqual([x for x in rezultat1], [x for x in sestej1])

        #pozitivna FLOAT, skalar INT
        a2 = BaseArray((10,),dtype = float, data=(1,2,3,4,5,6,7,8,9,10))
        skalar2 = 1
        rezultat2 = BaseArray((10,),dtype = float, data=(2,3,4,5,6,7,8,9,10,11))

        sestej2 = BaseArray.sestevanje(a2, skalar2)
        self.assertListEqual([x for x in rezultat2], [x for x in sestej2])

        #pozitivna FLOAT, skalar FLOAT
        a3 = BaseArray((10,),dtype = float, data=(1,2,3,4,5,6,7,8,9,10))
        skalar3 = 1
        rezultat3 = BaseArray((10,),dtype = float, data=(2,3,4,5,6,7,8,9,10,11))

        sestej3 = BaseArray.sestevanje(a3, skalar3)
        self.assertListEqual([x for x in rezultat3], [x for x in sestej3])

    def test_sestevanje_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(1, -5, 12, -8, -19, 3))
        b = BaseArray((2, 3), dtype=int, data=(1, -1, 1, -1, -1, -1))
        sestej = BaseArray.sestevanje(a, b)
        
        rezultat = BaseArray((2, 3), dtype=int, data=(2, -6, 13, -9, -20, 2))
        self.assertListEqual([x for x in rezultat], [x for x in sestej])

    def test_sestevanje_3D(self): #primer ena matrika FLOAT rezultat v FLOAT
        a = BaseArray((2, 3, 2), dtype=int, data=(1, -5, 12, -8, -19, 3, 1, -5, 12, -8, -19, 3))
        b = BaseArray((2, 3, 2), dtype=float, data=(1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1))
        sestej = BaseArray.sestevanje(a, b)
        
        rezultat = BaseArray((2, 3, 2), dtype=float, data=(2, -6, 13, -9, -20, 2, 2, -6, 13, -9, -20, 2))
        self.assertListEqual([x for x in rezultat], [x for x in sestej])

    # 2. ) ODSTEVANJE
    def test_odstevanje_skalar(self):
        # pozitivna INT, skalar INT 
        a = BaseArray((10,), dtype = int, data=(1,2,3,4,5,6,7,8,9,10))
        skalar = 1
        rezultat = BaseArray((10,),dtype = int, data=(0,1,2,3,4,5,6,7,8,9))

        odstej = BaseArray.odstevanje(a, skalar)
        self.assertEqual(int, odstej.dtype)    #tip float
        self.assertListEqual([x for x in rezultat], [x for x in odstej])

        # pozitivna INT, skalar FLOAT 
        a1 = BaseArray((10,),dtype = int, data=(1,2,3,4,5,6,7,8,9,10))
        skalar1 = 1.0
        rezultat1 = BaseArray((10,),dtype = float, data=(0,1,2,3,4,5,6,7,8,9))

        odstej1 = BaseArray.odstevanje(a1, skalar1)
        self.assertEqual(float, odstej1.dtype)    #tip float
        self.assertListEqual([x for x in rezultat1], [x for x in odstej1])

        #pozitivna FLOAT, skalar INT
        a2 = BaseArray((10,),dtype = float, data=(1,2,3,4,5,6,7,8,9,10))
        skalar2 = 1
        rezultat2 = BaseArray((10,),dtype = float, data=(0,1,2,3,4,5,6,7,8,9))

        odstej2 = BaseArray.odstevanje(a2, skalar2)
        self.assertEqual(float, odstej2.dtype)    #tip float
        self.assertListEqual([x for x in rezultat2], [x for x in odstej2])

        #pozitivna FLOAT, skalar FLOAT
        a3 = BaseArray((10,),dtype = float, data=(1,2,3,4,5,6,7,8,9,10))
        skalar3 = 1
        rezultat3 = BaseArray((10,),dtype = float, data=(0,1,2,3,4,5,6,7,8,9))

        odstej3 = BaseArray.odstevanje(a3, skalar3)
        self.assertEqual(float, odstej3.dtype)    #tip float
        self.assertListEqual([x for x in rezultat3], [x for x in odstej3])

    def test_odstevanje_2D(self):
        a = BaseArray((2, 3), dtype=float, data=(1, -5, 12, -8, -19, 3))
        b = BaseArray((2, 3), dtype=int, data=(1, -1, 1, -1, -1, -1))
        odstej = BaseArray.odstevanje(a, b)

        self.assertEqual(float, odstej.dtype)    #tip float
        rezultat = BaseArray((2, 3), dtype=int, data=(0, -4, 11, -7, -18, 4))
        self.assertListEqual([x for x in rezultat], [x for x in odstej])

    def test_odstevanje_3D(self): #primer ena matrika FLOAT rezultat v FLOAT
        a = BaseArray((2, 3, 2), dtype=int, data=(1, -5, 12, -8, -19, 3, 1, -5, 12, -8, -19, 3))
        b = BaseArray((2, 3, 2), dtype=float, data=(1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1))
        odstej = BaseArray.odstevanje(a, b)
        self.assertEqual(float, odstej.dtype)    #tip float

        rezultat = BaseArray((2, 3, 2), dtype=float, data=(0, -4, 11, -7, -18, 4,0, -4, 11, -7, -18, 4))
        self.assertListEqual([x for x in rezultat], [x for x in odstej])

    # 3. ) MNOZENJE
    def test_mnozenje_skalar(self):
        # pozitivna INT, skalar INT 
        a = BaseArray((10,), dtype = int, data=(1,2,3,4,5,6,7,8,9,10))
        skalar = 1
        rezultat = BaseArray((10,),dtype = int, data=(0,1,2,3,4,5,6,7,8,9))

        mnozi = BaseArray.odstevanje(a, skalar)
        self.assertEqual(int, mnozi.dtype)    #tip float
        self.assertListEqual([x for x in rezultat], [x for x in mnozi])
    
    def test_mnozenje_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(1, -5, 12, -8, -19, 3))
        b = BaseArray((2, 3), dtype=float, data=(1, -1, 1, -1, -1, -1))
        mnozi = BaseArray.mnozenje(a, b)

        self.assertEqual(float, mnozi.dtype)    #tip float
        rezultat = BaseArray((2, 3), dtype=int, data=(1,5,12,8,19,-3))
        self.assertListEqual([x for x in rezultat], [x for x in mnozi])

    def test_mnozenje_3D(self): #primer ena matrika FLOAT rezultat v FLOAT
        a = BaseArray((2, 3, 2), dtype=int, data=(1, -5, 12, -8, -19, 3, 1, -5, 12, -8, -19, 3))
        b = BaseArray((2, 3, 2), dtype=float, data=(1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1))
        mnozi = BaseArray.mnozenje(a, b)
        
        self.assertEqual(float, mnozi.dtype)    #tip float
        rezultat = BaseArray((2, 3, 2), dtype=float, data=(1,5,12,8,19,-3,1,5,12,8,19,-3))
        self.assertListEqual([x for x in rezultat], [x for x in mnozi])

    # 4. ) DELJENJE
    def test_deljenje_skalar(self):
        # pozitivna INT, skalar INT 
        a = BaseArray((10,), dtype = int, data=(1,2,3,4,5,6,7,8,9,10))
        skalar = 1
        rezultat = BaseArray((10,),dtype = int, data=(1,2,3,4,5,6,7,8,9,10))

        deljenje = BaseArray.deljenje(a, skalar)
        self.assertEqual(float, a.dtype)    #tip float
        self.assertListEqual([x for x in rezultat], [x for x in deljenje])
    
    def test_deljenje_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(1, -5, 12, -8, -19, 3))
        b = BaseArray((2, 3), dtype=int, data=(1, -1, 1, -1, -1, -1))
        deli = BaseArray.deljenje(a, b)
        self.assertEqual(float, a.dtype)    #tip float

        rezultat = BaseArray((2, 3), dtype=int, data=(1,5,12,8,19,-3))
        self.assertListEqual([x for x in rezultat], [x for x in deli])

    def test_deljenje_3D(self): #primer ena matrika FLOAT rezultat v FLOAT
        a = BaseArray((2, 3, 2), dtype=int, data=(1, -5, 12, -8, -19, 3, 1, -5, 12, -8, -19, 3))
        b = BaseArray((2, 3, 2), dtype=float, data=(1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1))
        deli = BaseArray.deljenje(a, b)
        self.assertEqual(float, a.dtype)    #tip float

        rezultat = BaseArray((2, 3, 2), dtype=float, data=(1,5,12,8,19,-3,1,5,12,8,19,-3))
        self.assertListEqual([x for x in rezultat], [x for x in deli])
    
    # 5. ) LOGARITMIRANJE
    def test_logaritmiranje_skalar(self):
        a = BaseArray((6,), dtype = int, data=(1,2,3,4,5,6))
        skalar = 2
        rezultat = BaseArray((6,),dtype = int, data=(0,1,1.58,2,2.32,2.58))

        logaritmiranje = BaseArray.logaritmiranje(a, skalar)
        self.assertEqual(float, a.dtype)    #tip float
        for i in range(1, 7):
            self.assertAlmostEqual(logaritmiranje[i], rezultat[i], 2)
    
    def test_logaritmiranje_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 5, 12, 8, 19, 3))
        b = BaseArray((2, 3), dtype=int, data=(2, 3, 4, 2, 2, 3))
        logaritmiranje = BaseArray.logaritmiranje(a, b)
        
        rezultat = BaseArray((2, 3), dtype=int, data=(0,1.46,1.79,3,4.24,1))
        
        self.assertEqual(float, a.dtype)    #tip float
        for i in range(1, 3):
            for j in range(1, 4):
                self.assertAlmostEqual(logaritmiranje[i, j], rezultat[i, j], 1)

    def test_logaritmiranje_3D(self): #primer ena matrika FLOAT rezultat v FLOAT
        a = BaseArray((2, 3, 2), dtype=int, data=(1, 5, 12, 8, 19, 3, 1, 5, 12, 8, 19, 3))
        b = BaseArray((2, 3, 2), dtype=float, data=(2, 3, 4, 2, 2, 3, 2, 3, 4, 2, 2, 3))
        logaritmiranje = BaseArray.logaritmiranje(a, b)
        
        rezultat = BaseArray((2, 3, 2), dtype=float, data=(0,1.46,1.79,3,4.24,1, 0,1.46,1.79,3,4.24,1))
        
        self.assertEqual(float, a.dtype)    #tip float
        for i in range(1, 3):
            for j in range(1, 4):
                for k in range(1, 3):
                    self.assertAlmostEqual(logaritmiranje[i, j, k], rezultat[i, j, k], 1)

    # 6. ) POTENCIRANJE
    def test_potenciranje_skalar(self):
        # pozitivna INT, skalar INT 
        a = BaseArray((10,), dtype = int, data=(1,2,3,4,5,6,7,8,9,10))
        skalar = 3
        rezultat = BaseArray((10,),dtype = int, data=(1,8,27,64,125,216,343,512,729,1000))

        potenciranje = BaseArray.potenciranje(a, skalar)
        self.assertEqual(float, potenciranje.dtype)    #tip float
        self.assertListEqual([x for x in rezultat], [x for x in potenciranje])
   
    def test_potenciranje_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 5, 12, -8, -2, 3))
        b = BaseArray((2, 3), dtype=int, data=(2, 4, 3, -1, 2, 3))
        potenciranje = BaseArray.potenciranje(a, b)
      
        self.assertEqual(float, a.dtype)    #tip float
        self.assertEqual(float, potenciranje.dtype)    #tip float

        rezultat = BaseArray((2, 3), dtype=int, data=(1,625,1728,-0.125, 4, 27))
        self.assertListEqual([x for x in rezultat], [x for x in potenciranje])

    def test_potenciranje_3D(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(1, 5, 12, -8, -2, 3, 1, 5, 12, -8, -2, 3))
        b = BaseArray((2, 3, 2), dtype=int, data=(2, 4, 3, -1, 2, 3, 2, 4, 3, -1, 2, 3))
        potenciranje = BaseArray.potenciranje(a, b)
      
        self.assertEqual(float, a.dtype)    #tip float
        self.assertEqual(float, potenciranje.dtype)    #tip float

        rezultat = BaseArray((2, 3, 2), dtype=int, data=(1,625,1728,-0.125, 4, 27, 1,625,1728,-0.125, 4, 27))
        self.assertListEqual([x for x in rezultat], [x for x in potenciranje])

    # 7. ) MATRIČNO MNOŽENJE
    def test_matricnoMnozenje_2D(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 5, 12, -8, -2, 3))
        b = BaseArray((3, 2), dtype=int, data=(2, 4, 3, -1, 2, 3))
        matricnoMnozenje = BaseArray.matricnoMnozenje(a, b)
      
        self.assertEqual(int, a.dtype)    #tip float
        self.assertEqual(int, matricnoMnozenje.dtype)    #tip float

        rezultat = BaseArray((2, 2), dtype=int, data=(41, 35, -16, -21))
        self.assertListEqual([x for x in rezultat], [x for x in matricnoMnozenje])

    def test_matricnoMnozenje_2D_error(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 5, 12, -8, -2, 3))
        b = BaseArray((2, 2), dtype=int, data=(2, 4, 3, -1))
        
        try:
            matricnoMnozenje = BaseArray.matricnoMnozenje(a, b)
        except:
            self.assertTrue(True)

        
    

if __name__ == '__main__':
    unittest.main()
