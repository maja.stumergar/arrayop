# pylint: disable=line-too-long
# pylint: disable=invalid-name

from typing import Tuple
from typing import List
from typing import Union
from math import log, exp
import array
import math


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    @property
    def izpisTabele(self):
        if len(self.__shape) == 1:
            for row in range(1, self.shape[0]+1):
                if self.dtype == float:
                    print('{:7.2f}'.format(self[row]), end='')
                else:
                    print('{:5d}'.format(self[row]), end='')
        elif len(self.__shape) == 2:
            for row in range(1, self.shape[0]+1):
                for elem in range(self.shape[1]):
                    if self.dtype == float:
                        print('{:7.2f}'.format(self[row, elem+1]), end='')
                    else:
                        print('{:5d}'.format(self[row, elem+1]), end='')
                print()
        elif len(self.shape) == 3:
            for x in range(self.shape[0]):
                print('Plast:', x)
                for y in range(self.shape[1]):
                    for z in range(self.shape[2]):
                        if self.dtype == float:
                            print('{:7.2f}'.format(self[x+1, y+1, z+1]), end='')
                        else:
                            print('{:5d}'.format(self[x+1, y+1, z+1]), end='')
                    print()
#format :  %[flags][width][.precision]type 5d == s 5 stevili ce je stevilo 1 bo kot 00001 pomeni 4x prazno mesto

    def MergeSort(self, opcija = "vrstica"):
        velikost = len(self.__shape)

        if velikost == 1:
            mergeSort(self.__data, len(self.__data))
        elif velikost == 2:
            if opcija == "vrstica": #vrstica po vrstica
                for i in range(1, self.shape[-2] + 1): #vrstica
                    vrstica = []
                    for j in range(1, self.shape[-1] + 1): #stolpec
                        vrstica.append(self[i, j])
                    mergeSort(vrstica, len(vrstica))
                    for j in range(1, len(vrstica) + 1):
                        self[i, j] = vrstica[j-1] #slef = sortirani elem.
            if opcija == "stolpec":
                for i in range(1, self.shape[-1] + 1): #stolpec
                    stolpec = []
                    for j in range(1, self.shape[-2] + 1): #vrstice
                        stolpec.append(self[j, i])
                    mergeSort(stolpec, len(stolpec))
                    for j in range(1, len(stolpec) + 1):
                        self[j, i] = stolpec[j-1]
        else:
            raise Exception("Sortiranje je dovoljeno samo nad 1D in 2D!")


    def iskanjeElementa(self, iskanaVrednost):
        velikost = len(self.__shape)
        rezultat = "("
        temp = 1

        if velikost == 1:
            for i in range(1, self.shape[0] + 1):
                if self[i] == iskanaVrednost:
                    rezultat += ('({0}), '.format(i))
                    temp = 0
        if velikost == 2:
            for i in range(1, self.shape[0] + 1):
                for j in range(1, self.shape[1] + 1):
                    if self[i, j] == iskanaVrednost:
                        rezultat += ('({0}, {1}), '.format(i, j))
                        temp=0
        if velikost == 3:
             for i in range(1, self.shape[0] + 1):
                for j in range(1, self.shape[1] + 1):
                    for k in range(1, self.shape[2] + 1):
                        if self[i, j, k] == iskanaVrednost:
                            rezultat += ('({0}, {1}, {2}), '.format(i, j, k))
                            temp=0
        
        if velikost == 0 or velikost > 3:
             raise ValueError("Iskanje je dovoljeno samo nad 1D, 2D in 3D!")

        rezultat = rezultat[:-2] #iz rezultata odstranim ", ", ki dodam pri vsakem izpisi = SLICING
        rezultat += ")"
        
        if temp == 1:
            print("Vrednost ni bila najdena")
        else:
            print(rezultat)
            return rezultat
           
    def sestevanje(self, other):
        velikost = len(self.__shape)
        velikostPodatki = len(self.__data)

        if velikost == 0 or velikost > 3:
            raise ValueError("Sestevanje je dovoljeno samo nad 1D, 2D in 3D!")

        if type(other) == BaseArray: # dve matriki
            if other.shape == self.shape: # preveri velikost matrik
                if other.dtype == float:
                    self.__dtype = float
                for i in range(velikostPodatki):
                    self.__data[i] += other.__data[i]
            else:
                raise ValueError("Velikost matrik napacna!")
        elif type(other) == int or type(other) == float: #matrika s skalarjem
            if type(other) == float:
                self.__dtype = float
            for i in range(velikostPodatki):
                self.__data[i] += other
        return self

    def odstevanje(self, other):
        velikost = len(self.__shape)
        velikostPodatki = len(self.__data)

        if velikost == 0 or velikost > 3:
            raise ValueError("Odstevanje je dovoljeno samo nad 1D, 2D in 3D!")

        if type(other) == BaseArray: # dve matriki
            if other.shape == self.shape: # preveri velikost matrik
                if other.dtype == float:
                    self.__dtype = float
                for i in range(velikostPodatki):
                    self.__data[i] -= other.__data[i]
        elif type(other) == int or type(other) == float: #matrika s skalarjem
            if type(other) == float:
                self.__dtype = float
            for i in range(velikostPodatki):
                self.__data[i] -= other
        return self

    def mnozenje(self, other):
        velikost = len(self.__shape)
        velikostPodatki = len(self.__data)

        if velikost == 0 or velikost > 3:
            raise ValueError("Mnozenje je dovoljeno samo nad 1D, 2D in 3D!")

        if type(other) == BaseArray: # dve matriki
            if other.shape == self.shape: # preveri velikost matrik
                if other.dtype == float:
                    self.__dtype = float
                for i in range(velikostPodatki):
                    self.__data[i] *= other.__data[i]
        elif type(other) == int or type(other) == float: #matrika s skalarjem
            if type(other) == float:
                self.__dtype = float
            for i in range(velikostPodatki):
                self.__data[i] *= other
        return self

    def deljenje(self, other):
        velikost = len(self.__shape)
        velikostPodatki = len(self.__data)
        self.__dtype = float
        
        if velikost == 0 or velikost > 3:
            raise ValueError("Deljenje je dovoljeno samo nad 1D, 2D in 3D!")

        if type(other) == BaseArray: # dve matriki
            if other.shape == self.shape: # preveri velikost matrik
                for i in range(velikostPodatki):
                    if other.__data[i] != 0:
                        self.__data[i] /= other.__data[i]
                    else:
                         raise ValueError("Deljenje z številom 0!")
        elif type(other) == int or type(other) == float: #matrika s skalarjem
            for i in range(velikostPodatki):
                if other != 0:
                    self.__data[i] /= other
                else:
                        raise ValueError("Deljenje z številom 0!")
        return self

    def logaritmiranje(self, other):
        velikost = len(self.__shape)
        velikostPodatki = len(self.__data)
        self.__dtype = float
        
        #self = osnova = vecja od 0
        #other = stopnja = vecja od nic in nesme biti enaka 1

        if velikost == 0 or velikost > 3:
            raise ValueError("Logaritmiranje je dovoljeno samo nad 1D, 2D in 3D!")

        if type(other) == BaseArray: # dve matriki
            if other.shape == self.shape: # preveri velikost matrik
                for i in range(velikostPodatki):
                    if other.__data[i] > 0 and other.__data !=1 and self.__data[i]>0:
                        self.__data[i] = log(self.__data[i], other.__data[i])
                    else:
                         raise ValueError("Ni mozno logaritmirati!")
        elif type(other) == int or type(other) == float: #matrika s skalarjem
            for i in range(velikostPodatki):
                if other > 0 and other != 1 and self.__data[i] > 0:
                    self.__data[i] = log(self.__data[i], other)
                else:
                        raise ValueError("Deljenje z številom 0!")
        return self

    def potenciranje(self, other):
        velikost = len(self.__shape)
        velikostPodatki = len(self.__data)
        self.__dtype = float
        
        #self = osnova = vecja od 0
        #other = stopnja = vecja od nic in nesme biti enaka 1
        # 0^stopnjo = 0; 1^stopnjo = 1

        if velikost == 0 or velikost > 3:
            raise ValueError("Potenciranje je dovoljeno samo nad 1D, 2D in 3D!")

        if type(other) == BaseArray: # dve matriki
            if other.shape == self.shape: # preveri velikost matrik
                for i in range(velikostPodatki):
                    self.__data[i] = math.pow(self.__data[i], other.__data[i])
        elif type(other) == int or type(other) == float: #matrika s skalarjem
            for i in range(velikostPodatki):
                self.__data[i] = math.pow(self.__data[i], other)
        return self

    def matricnoMnozenje(self, other):
        velikost = len(self.__shape)
        self.__dtype = int
        
        if other.dtype is float:
            self.__dtype = float

        tip = self.__dtype

        if velikost != 2 and other.shape.__len__() != 2:
                raise ValueError("Matriki nista 2D ")

        if type(other) == BaseArray: # dve matriki
            if self.shape[1] == other.shape[0]: #pogoj matricnega mnozenja
                res = BaseArray((self.shape[0],other.shape[1]), dtype=tip)
                for i1 in range(self.shape[0]):
                    for i2 in range(other.shape[1]):
                        for i3 in range(other.shape[0]):
                            res[i1+1, i2+1] += self[i1+1, i3+1] * other[i3+1, i2+1]
                return res
            else:
                raise ValueError("Matriki nista pravih dimenzij")


    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data


def mergeSort(podatki, podatkiVelikost):
        if podatkiVelikost > 1:
            mid = podatkiVelikost // 2 #deli, pobrisi decimalke
            lefthalf = podatki[:mid]
            righthalf = podatki[mid:]
            
            levo = mid
            desno = podatkiVelikost - levo

            mergeSort(lefthalf, levo)
            mergeSort(righthalf, desno)
            i = 0
            j = 0
            k = 0

            while i < levo and j < desno:
                if lefthalf[i] < righthalf[j]:
                    podatki[k] = lefthalf[i]
                    i = i + 1
                else:
                    podatki[k] = righthalf[j]
                    j = j + 1
                k = k + 1
            while i < levo:
                podatki[k] = lefthalf[i]
                i = i + 1
                k = k + 1
            while j < desno:
                podatki[k] = righthalf[j]
                j = j + 1
                k = k + 1


def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True
